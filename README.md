# Test the image export capabilities introduced in plotly.py 3.2.0

## Installation

You can upgrade or install via pip but recommended to use conda because you have to install plotly-orca as dependency.

### Install via conda

```
conda install -c plotly plotly plotly-orca
```


### Install via pip

```
pip install plotly --upgrade

npm install -g electron@1.8.4 orca
```

### How to use

Please check the Jupyter notebook for use case.
